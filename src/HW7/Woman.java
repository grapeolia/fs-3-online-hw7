package HW7;

import java.util.Map;

public final class Woman extends Human{
    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, Pet pet, Map<String,String> schedule) {
        super(name, surname, year, iq, pet, schedule);
    }

    @Override
    public void greetPets() {
        super.greetPets();
        System.out.println("Woman said");
    }

    public void makeup(){
        System.out.println("I've finished make up. I'm beautiful!");
    }

    @Override
    public String toString() {
        return "Woman{" +
                "name='" + super.getName() + '\'' +
                ", surname='" + super.getSurname() + '\'' +
                ", year=" + super.getYear() +
                ", iq=" + super.getIq() +
                ", schedule=" + super.getSchedule() +
                '}';
    }


}
