package HW7;

import java.util.*;

public class HappyFamily {
    public static void main(String[] args) {
        Family family1 = new Family();
        Map<String,String> schedule = setUpDailySchedule();
        Set<String> boriaHabits = new HashSet<>();
        Set<String> robHabits = new HashSet<>();
        List<Human> children = new ArrayList<>();
        Set<Pet> pets = new HashSet<>();
        Woman mother1 = new Woman("Mother", "MotherLastName", 1979);
        Man father1 = new Man("Father", "FatherLastName", 1974);
        Human child1 = new Human("Child1", "ChildLastName", 2000);
        Human childToAdd = new Human("Child2","Child2Surname", 2019);
        boriaHabits.add("eat");
        boriaHabits.add("sleep");
        robHabits.add("eat");
        robHabits.add("sleep");
        robHabits.add("jump");
        Pet catBoria = new DomesticCat("Boria",2, 46, boriaHabits);
        Pet dogRob = new Dog("Rob",3,67, robHabits);
        father1.setSchedule(schedule);
        family1.setMother(mother1);
        family1.setFather(father1);
        children.add(child1);
        family1.setChildren(children);
        family1.addChild(childToAdd);
        family1.deleteChild(0);
        family1.addPet(dogRob);
        Family familyNew = new Family(mother1,father1,children);
        familyNew.deleteChild(child1);
        System.out.println(familyNew.getPets());
        family1.getMother().makeup();
        family1.getMother().setSchedule(schedule);
        System.out.println(family1.getMother().getSchedule());
        family1.getFather().repairCar();
        System.out.println(family1.deleteChild(5));
    }

    public static Map<String,String> setUpDailySchedule(){
        Map<String,String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY.name(),"Get Enough Sleep");
        schedule.put(DayOfWeek.TUESDAY.name(),"Rise Early");
        schedule.put(DayOfWeek.WEDNESDAY.name(),"Meditate");
        schedule.put(DayOfWeek.THURSDAY.name(), "Workout");
        schedule.put(DayOfWeek.FRIDAY.name(),"Eat A Good Breakfast");
        schedule.put(DayOfWeek.SATURDAY.name(),"Take A Nap");
        schedule.put(DayOfWeek.SUNDAY.name(), "Take Breaks To Re-energize");

        return schedule;
    }

}